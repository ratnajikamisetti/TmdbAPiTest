package org.example;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Main {
    public static void main(String[] args) {
        String apiKey = "f972f0ca80dcab68ba49822c1378170b"; // Replace with your TMDb API key
        TmdbApi tmdbApi = new TmdbApi(apiKey);

        String query = "nun";
        JSONObject movieData = tmdbApi.searchMovie(query);

        if (movieData != null) {
            JSONArray results = (JSONArray) movieData.get("results");
            for (Object result : results) {
                JSONObject movie = (JSONObject) result;
                String title = (String) movie.get("title");
                String releaseDate = (String) movie.get("release_date");
                System.out.println("Title: " + title);
                System.out.println("Release Date: " + releaseDate);
            }
        }
    }

}
