package org.example;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class TmdbApi {

    private final String apiKey;
    public TmdbApi(String apiKey){
        this.apiKey=apiKey;
    }
    public JSONObject searchMovie(String query){
        try{
            HttpClient httpClient= HttpClients.createDefault();
            HttpGet httpGet=new HttpGet("https://api.themoviedb.org/3/search/movie?api_key=" + apiKey + "&query=" + query);
            HttpResponse response=httpClient.execute(httpGet);
            JSONParser parser = new JSONParser();
            JSONObject jsonResponse = (JSONObject) parser.parse(EntityUtils.toString(response.getEntity()));

            return jsonResponse;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        }
    }

